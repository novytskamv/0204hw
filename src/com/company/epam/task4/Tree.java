package com.company.epam.task4;

import com.company.epam.task4.Node;

public class Tree {

  private Node root;

  public Node find(int key) {
    Node current = root;
    if (root != null) {
      while (key != current.iData) {
        if (key < current.iData) {
          current = current.leftChild;
        } else {
          current = current.rightChild;
        }
        if (current == null) {
          return null;
        }
      }
    }
    return current;
  }

  public void insert(int dd) {
    Node newNode = new Node(dd);
    Node current = this.root;
    if (root == null) {
      root = newNode;
      return;
    }
    while (true) {
      if (dd > current.iData) {
        if (current.rightChild == null) {
          current.rightChild = newNode;
          return;
        }
        current = current.rightChild;
      } else if (dd < current.iData) {
        if (current.leftChild == null) {
          current.leftChild = newNode;
          return;
        }
        current = current.leftChild;
      }
    }
  }

  public boolean delete(int key) {
    Node current = root;
    Node parent = root;
    if (root.iData == key && root.leftChild == null && root.rightChild == null) {
      root = null;
      return true;
    } else if(root.iData==key&&root.leftChild==null){
      root=root.rightChild;
      return true;
    }else if(root.iData==key&&root.rightChild==null){
      root = root.leftChild;
      return true;
    } else if(root.iData==key) {
      Node currentBefore = root.leftChild;
      root = root.rightChild;;
      current = root;
      while (current != null) {
        parent = current;
        current = current.leftChild;
      }
      parent.leftChild = currentBefore;
      return true;
    }
    while (current.iData != key) {
      if (key < current.iData) {
        if (current.leftChild == null) {
          return false;
        }
        parent = current;
        current = current.leftChild;
      } else if (key > current.iData) {
        if (current.rightChild == null) {
          return false;
        }
        parent = current;
        current = current.rightChild;
      }
    }
    if (current.rightChild == null && current.leftChild == null) {
      if (current == parent.rightChild) {
        parent.rightChild = null;
      } else {
        parent.leftChild = null;
      }
    } else {
      if (current.leftChild != null && current.rightChild != null) {
        if(current==parent.leftChild) {
          parent.leftChild = current.rightChild;
          Node currentBefore = current.leftChild;
          current = parent.leftChild; //
          while (current != null) {
            parent = current;
            current = current.leftChild;
          }
          parent.leftChild = currentBefore;
        } else {
          parent.rightChild = current.rightChild;
          Node currentBefore = current.leftChild;
          current = parent.rightChild;
          while (current != null) {
            parent = current;
            current = current.leftChild;
          }
          parent.leftChild = currentBefore;
        }
      } else if(current.leftChild!=null){
        parent.leftChild = current.leftChild;
      } else {
        parent.leftChild=current.rightChild;
      }
    }
    return true;
  }

  private String print( Node node, String prefix)
  {
    if( node == null )
      return prefix + "null\n";

    String result = prefix + node.iData + "\n";
    result += print( node.rightChild, prefix + "  |" );
    result += print( node.leftChild,  prefix + "   " );
    return result;
  }

  public String toString()
  {
    return print( root, "" );
  }

}
