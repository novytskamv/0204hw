package com.company.epam.task4;

public class MyTreeMap {


  public static void main(String[] args) {

    Tree tree = new Tree();
    tree.insert(50);
    tree.insert(84);
    tree.insert(78);
    tree.insert(100);
    tree.insert(75);
    tree.insert(80);
    tree.insert(62);
    tree.insert(41);
    tree.insert(37);
    tree.insert(48);
    tree.insert(43);
    tree.insert(38);
    tree.insert(17);
    tree.insert(26);

    tree.delete(99);

    System.out.println("================================================");
    System.out.println("               MY BINARY TREE                   ");
    System.out.println("================================================");
    System.out.println(tree);
    System.out.println("Tried to delete 78\n");
    System.out.println("================================================");
    System.out.println("                MY BINARY TREE                  ");
    System.out.println("================================================");
    tree.delete(78);
    System.out.println(tree);
    System.out.println("Tried to delete root 50\n");
    System.out.println("================================================");
    System.out.println("                MY BINARY TREE                  ");
    System.out.println("================================================");
    tree.delete(50);
    System.out.println(tree);

    System.out.println("Find key 100");
    Node found = tree.find(100);
    if (found != null) {
      System.out.println("Is found");
    } else {
      System.out.println("Not found");
    }

  }

}
