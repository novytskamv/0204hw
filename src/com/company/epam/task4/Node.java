package com.company.epam.task4;

public class Node{

  int iData;
  double fData;

  Node leftChild;
  Node rightChild;

  public Node( int iData) {
    this.iData = iData;
  }

}
