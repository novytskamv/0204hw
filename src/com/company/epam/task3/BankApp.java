package com.company.epam.task3;

import java.util.Scanner;

public class BankApp {

  enum MenuOptions
  {
    Z("Continue"), A("Add Bank"), B("Add Branch"), C("Add Customer"),
    D("Make Transaction"), E("Display Information"), Q("Quit");


    private final String meaning;
    MenuOptions(String meaning) {
      this.meaning = meaning;
    }

    public String getMeaning()
    {
      return meaning;
    }

  }

  private static Scanner input = new Scanner(System.in);

  public static void menu()
  {
    System.out.println("\nEnter:");
    System.out.println("\tA. Create Bank");
    System.out.println("\tB. Add a branch to a Bank");
    System.out.println("\tC. Add a customer to a Branch");
    System.out.println("\tD. Make a transaction with a customer");
    System.out.println("\tE. Display Banks, Branches, Customers, and Transactions.");
    System.out.println("\tQ. Quit Application.");
    System.out.print("\nSelection -> ");

  }


  public static void main(String[] args) {

    System.out.println("Welcome to Lviv Banking Application.");
    MenuOptions menuOptions = MenuOptions.Z;

    while (menuOptions != MenuOptions.Q)
      try
      {
        menu();


        menuOptions = MenuOptions.valueOf(input.nextLine());


        switch (menuOptions)
        {
          case A:
            System.out.println("===================================================");
            System.out.println("The procedure of creating bank...Please wait...");
            System.out.println("===================================================");
            break;

          case B:
            System.out.println("===================================================");
            System.out.println("The procedure of creating branch...Please wait...");
            System.out.println("===================================================");
            break;

          case C:
            System.out.println("===================================================");
            System.out.println("The procedure of creating customer...Please wait...");
            System.out.println("===================================================");
            break;

          case D:
            System.out.println("===================================================");
            System.out.println("The procedure of making transaction... Please wait...");
            System.out.println("===================================================");
            break;

          case E:
            System.out.println("===================================================");
            System.out.println("Displaying...");
            System.out.println("===================================================");
            break;

          case Q:
            System.out.println("===================================================");
            System.out.println("Goodbye.");
            System.out.println("===================================================");
            break;

          default:
            System.out.println("Selection out of range. Try again");
        }
      }
      catch (IllegalArgumentException e)
      {
        System.out.println("Selection out of range. Try again:");
      }



  }
}

